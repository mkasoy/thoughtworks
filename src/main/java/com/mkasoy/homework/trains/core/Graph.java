package com.mkasoy.homework.trains.core;

import com.mkasoy.homework.trains.beans.Road;
import com.mkasoy.homework.trains.constant.SystemConstant;
import com.mkasoy.homework.trains.util.FileUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <B>说       明</B>: 根据输入生成地图
 *
 * @author 作  者  名：zhengzhipeng
 * E-mail ：zhengzhipeng@vrvmail.com.cn
 * @date 创 建 时 间：15:48 2019/8/17
 * @modified
 */
public class Graph {
    public static HashMap<String, Road> roads = new HashMap<>();
    static {
        List<String> lineList = FileUtil.readFileByLine(SystemConstant.INPUT_FILE_PATH);
        /*题目给定规则*/
        if (lineList.size() > 0) {
            //System.out.println("正确读取输入文件！");
            String[] roadList = lineList.get(0).split(", ");
            for (String roadStr : roadList) {
                Road road = new Road(roadStr);
                roads.put(road.getRoadName(), road);
            }
            //System.out.println("图加载成功！");
        }
    }
}
