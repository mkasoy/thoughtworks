package com.mkasoy.homework.trains.enums;

/**
 * <B>说       明</B>:比较对象枚举类
 *
 * @author 作  者  名：zhengzhipeng
 * E-mail ：zhengzhipeng@vrvmail.com.cn
 * @date 创 建 时 间：13:43 2019/8/18
 * @modified
 */
public enum Type {
    STOPS("stops", 1), DISTANCE("distance", 2);
    private String name;
    private int value;

    Type(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
