package com.mkasoy.homework.trains.constant;

/**
 * <B>说       明</B>:
 *
 * @author 作  者  名：zhengzhipeng
 * E-mail ：zhengzhipeng@vrvmail.com.cn
 * @date 创 建 时 间：15:30 2019/8/17
 * @modified
 */
public final class SystemConstant {
    /*输入文件路径*/
    public static final String INPUT_FILE_PATH = "src/main/resources/map.txt";
    public static final String NO_SUCH_ROUTE = "NO SUCH ROUTE";
}
