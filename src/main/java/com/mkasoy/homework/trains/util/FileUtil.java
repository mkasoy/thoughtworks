package com.mkasoy.homework.trains.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <B>说       明</B>:文件读取类
 *
 * @author 作  者  名：zhengzhipeng
 * E-mail ：zhengzhipeng@vrvmail.com.cn
 * @date 创 建 时 间：14:58 2019/8/17
 * @modified
 */
public class FileUtil {
    public static List<String> readFileByLine(String fileName) {
        ArrayList<String> arrayList = new ArrayList<>();
        File file = null;
        InputStreamReader inputReader = null;
        BufferedReader bf = null;
        //System.out.println(System.getProperty("user.dir"));
        try {
            file = new File(fileName);
            if (file.exists()) {
                inputReader = new InputStreamReader(new FileInputStream(file));
                bf = new BufferedReader(inputReader);
                // 按行读取字符串
                String str = null;
                while ((str = bf.readLine()) != null) {
                    arrayList.add(str);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputReader != null) {
                try {
                    inputReader.close();
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }
            if (bf != null) {
                try {
                    bf.close();
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }
        }
        return arrayList;
    }
}
