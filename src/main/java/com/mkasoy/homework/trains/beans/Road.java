package com.mkasoy.homework.trains.beans;

/**
 * <B>说       明</B>:车次，即输入中存在的路劲
 *
 * @author 作  者  名：zhengzhipeng
 * E-mail ：zhengzhipeng@vrvmail.com.cn
 * @date 创 建 时 间：12:22 2019/8/17
 * @modified
 */
public class Road {
    /*路名*/
    private String roadName;
    /*起点*/
    private char sStation;
    /*终点*/
    private char eStation;
    /*距离*/
    private int distance;

    public Road() {
    }

    public Road(String abbr){
        /*基于题目要求解析*/
        this.sStation = abbr.charAt(0);
        this.eStation = abbr.charAt(1);
        this.distance = abbr.charAt(2) - '0';
        this.roadName = abbr.substring(0,2);
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public char getsStation() {
        return sStation;
    }

    public void setsStation(char sStation) {
        this.sStation = sStation;
    }

    public char geteStation() {
        return eStation;
    }

    public void seteStation(char eStation) {
        this.eStation = eStation;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Road{");
        sb.append("roadName='").append(roadName).append('\'');
        sb.append(", sStation=").append(sStation);
        sb.append(", eStation=").append(eStation);
        sb.append(", distance=").append(distance);
        sb.append('}');
        return sb.toString();
    }
}
