package com.mkasoy.homework.trains.beans;

import com.mkasoy.homework.trains.enums.Operate;
import com.mkasoy.homework.trains.enums.Type;

/**
 * <B>说       明</B>:操作参数，路由搜索中的条件
 *
 * @author 作  者  名：zhengzhipeng
 * E-mail ：zhengzhipeng@vrvmail.com.cn
 * @date 创 建 时 间：13:24 2019/8/18
 * @modified
 */
public class Param {
    private Operate op;
    private Type type;
    private int value;

    public Param() {
    }

    public Param(Operate op, Type type, int value) {
        this.op = op;
        this.type = type;
        this.value = value;
    }

    public Operate getOp() {
        return op;
    }

    public void setOp(Operate op) {
        this.op = op;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
