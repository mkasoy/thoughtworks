package com.mkasoy.homework.trains.beans;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * <B>说       明</B>:线路，包括途径站点/距离/停靠次数信息
 *
 * @author 作  者  名：zhengzhipeng
 * E-mail ：zhengzhipeng@vrvmail.com.cn
 * @date 创 建 时 间：12:49 2019/8/18
 * @modified
 */
public class Trip {
    private int stops;
    private int distance;
    private LinkedList<String> stations = new LinkedList<>();

    public int getStops() {
        return stops;
    }

    public void setStops(int stops) {
        this.stops = stops;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public LinkedList<String> getStations() {
        return stations;
    }

    public void setStations(LinkedList<String> stations) {
        this.stations = stations;
    }

    public void clone(Trip dst){
        dst.stops = this.stops;
        dst.distance = this.distance;
        for(Iterator iter = this.stations.iterator(); iter.hasNext();)
            dst.stations.add((String) iter.next());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Trip{");
        sb.append("stops=").append(stops);
        sb.append(", distance=").append(distance);
        sb.append(", stations=").append(stations);
        sb.append('}');
        return sb.toString();
    }
}
